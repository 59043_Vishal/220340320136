import java.util.*;
class Reverse{

	static Node head;
	static class Node{
		int data;
		Node next;
		
		Node(int d){
			data=d;
			next=null;
		}
	}
	
	Node reverse(Node n){
		Node prev=null;
		Node curr=n;
		Node next=null;
		
		while(curr!=null){
			next=curr.next;
			curr.next=prev;
			prev=curr;
			curr=next;
		
		}
		n=prev;
		return n;
	
	}
	
	void print(Node n){
		while(n!=null){
			System.out.print(n.data+" ");
			n=n.next;
		}
		
	}

	
	public static void main(String args[]){
		Reverse r=new Reverse();
		r.head=new Node(5);
		r.head.next=new Node(1);
		r.head.next.next=new Node(2);
		r.head.next.next.next=new Node(3);
		r.head.next.next.next.next=new Node(4);
		head=r.reverse(head);
		
		System.out.println("Reversed list is");
		r.print(head);
		
		
		
	}
}