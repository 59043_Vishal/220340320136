import java.util.*;
class Stack{
	int arr[];
	int size;
	int m;
	int n;
	
	
	Stack(int a){
		arr=new int [a];
		size=a;
		m=-1;
		n=size;
	}
	
	void push1(int i){
		if(m<n-1){
			m++;
			arr[m]=i;
		}else {
			System.out.println("Stack is Overflow");
			System.exit(1);
		}
	}
	
	void push2(int i){
		if(m<n-1){
			n--;
			arr[n]=i;
		}else {
			System.out.println("Stack is Overflow");
			System.exit(1);
		}
	}
	
	int pop1(){
		if(m>=0){
			int i=arr[m];
			m--;
			return i;
		}else{
			System.out.println("Stack is underflow");
			System.exit(1);
		}return 0;
	}
	
	int pop2(){
		if(n<size){
			int i=arr[n];
			n++;
			return i;
		}else{
			System.out.println("Stack is underflow");
			System.exit(1);
		}return 0;
	}


	public static void main(String args[]){
		Stack s1=new Stack(5);
		s1.push1(5);
		s1.push2(10);
		s1.push2(15);
		s1.push1(11);
		s1.push2(7);
		
		
		System.out.println("Popped element from stack1 is"+ s1.pop1());
		
		s1.push2(40);
		System.out.println("Popped element from stack2 is"+ s1.pop2());
}
}